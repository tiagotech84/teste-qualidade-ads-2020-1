package br.ucsal.ads20201.testequalidade.aula05;

public class Calculo {
	
	public static long calcularFatorial(int n) {
		long fat = 1;
		for (int i = 1; i < n; i++) {
			fat *= i;
		}
		if (n == 3) {
			fat /= 0;
		}
		return fat;
	}
	
}
