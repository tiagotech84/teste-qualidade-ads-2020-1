package br.ucsal.ads20201.testequalidade.aula05;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class CalculoTest {

	
	
	@ParameterizedTest(name = "{index} calcularFatorial({0})")
	@MethodSource("fornecerDadosTest")
	public void testarFatorial(int n, long fatorialEsperado) {
		long fatorialAtual = Calculo.calcularFatorial(n);
		
		assertEquals(fatorialEsperado, fatorialAtual, "mensagem de erro customizada!");
	}
	
	private static Stream<Arguments> fornecerDadosTest() {
	    return Stream.of(
	      Arguments.of(3, 6),
	      Arguments.of(0, 1),
	      Arguments.of(4, 24),
	      Arguments.of(1, 1)
	    );
	}

	
	
	
	
	@Test
	public void testarUsandoLambda() {
		assertTrue(Stream.of(10, 8)
				.mapToInt(i -> i)
				.sum() == 18, () -> "A soma deve ser 18.");
	}

	@Test
	public void testarComMultiplosAsserts() {
		assertAll("fatoriais",
				 () -> assertEquals(1,Calculo.calcularFatorial(0)),
				 () -> assertEquals(1,Calculo.calcularFatorial(1)),
				 () -> assertEquals(6,Calculo.calcularFatorial(3)),
				 () -> assertEquals(24,Calculo.calcularFatorial(4))
		    );
	}
	
}
