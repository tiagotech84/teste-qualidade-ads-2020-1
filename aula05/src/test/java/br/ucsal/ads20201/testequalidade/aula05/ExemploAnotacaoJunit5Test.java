package br.ucsal.ads20201.testequalidade.aula05;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class ExemploAnotacaoJunit5Test {

	@BeforeAll
	public static void beforeAll() {
		System.out.println("@BeforeAll");
	}
	
	@BeforeEach
	public void beforeEach() {
		System.out.println("	@BeforeEach");
	}
	
	@Test
	@DisplayName("Teste de alguma coisa que voc� gostaria...")
	public void test1() {
		System.out.println("		@Test1");
	}
	
	@Test
	@Disabled
	public void test2() {
		System.out.println("		@Test2");
	}
	
	@Test
	public void test3() {
		System.out.println("		@Test3");
	}
	
	@AfterEach
	public void afterEach() {
		System.out.println("	@AfterEach");
	}
	
	@AfterAll
	public static void afterAll() {
		System.out.println("@AfterAll");
	}
}
