package br.ucsal.ads20201.testequalidade.aula04;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class CalculoParametrizadoTest {

	@Parameters(name = "{index} - calcularFatorial({0})")
	public static Collection<Object[]> obterCasosDeTeste() {
		return Arrays.asList(new Object[][] { { 0, 1 }, { 1, 1 }, { 3, 6 }, { 4, 24 }, { 5, 120 } });
	}

	// public static Collection<Object[]> obterCasosDeTeste() {
	// List<Object[]> casosTeste = new ArrayList<>();
	// casosTeste.add(new Object[] { 0, 1 });
	// casosTeste.add(new Object[] { 1, 1 });
	// casosTeste.add(new Object[] { 3, 6 });
	// casosTeste.add(new Object[] { 4, 24 });
	// casosTeste.add(new Object[] { 5, 120 });
	// return casosTeste;
	// }

	@Parameter(0)
	public int n;

	@Parameter(1)
	public long fatorialEsperado;

	@Test
	public void testarFatorial() {
		long fatorialAtual = Calculo.calcularFatorial(n);
		Assert.assertEquals(fatorialEsperado, fatorialAtual);
	}

}