package br.ucsal.ads20201.testequalidade.aula04;

public class CalculoMinhaInfraestruturaTest {

	public static void main(String[] args) {
		long fatorialAtual;
		try {
			fatorialAtual = Calculo.calcularFatorial(0);
			if (fatorialAtual == 1) {
				System.out.println("fatorial(0). SUCESSO");
			} else {
				System.out.println("fatorial(0). FALHA. Fatorial atual=" + fatorialAtual);
			}
		} catch (Exception e) {
			System.out.println("fatorial(0). ERRO=" + e.getMessage());
		}

		try {
			fatorialAtual = Calculo.calcularFatorial(3);
			if (fatorialAtual == 6) {
				System.out.println("fatorial(3). SUCESSO");
			} else {
				System.out.println("fatorial(3). FALHA Fatorial atual=" + fatorialAtual);
			}
		} catch (Exception e) {
			System.out.println("fatorial(3). ERRO=" + e.getMessage());
		}

		try {
			fatorialAtual = Calculo.calcularFatorial(5);
			if (fatorialAtual == 120) {
				System.out.println("fatorial(5). SUCESSO");
			} else {
				System.out.println("fatorial(5). FALHA. Fatorial atual=" + fatorialAtual);
			}
		} catch (Exception e) {
			System.out.println("fatorial(5). ERRO=" + e.getMessage());
		}
	}

}
