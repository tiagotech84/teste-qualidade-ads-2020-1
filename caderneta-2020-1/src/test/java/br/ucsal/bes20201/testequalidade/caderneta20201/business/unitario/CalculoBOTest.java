package br.ucsal.bes20201.testequalidade.caderneta20201.business.unitario;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import br.ucsal.bes20201.testequalidade.caderneta20201.business.CalculoBO;
import br.ucsal.bes20201.testequalidade.caderneta20201.business.ConceitoEnum;
import br.ucsal.bes20201.testequalidade.caderneta20201.business.MediaForaFaixaException;

public class CalculoBOTest {

	@Test
	@DisplayName("Testar definição de conceito para média 2.")
	public void testarDefinicaoConceitoMedia2() throws MediaForaFaixaException {
		CalculoBO calculoBO = new CalculoBO();
		Double media = 2d;
		ConceitoEnum conceitoEsperado = ConceitoEnum.REPROVADO;
		ConceitoEnum conceitoAtual = calculoBO.definirConceito(media);
		assertEquals(conceitoEsperado, conceitoAtual);
	}

	@Test
	@DisplayName("Testar definição de conceito para média 20.")
	public void testarDefinicaoConceitoMedia20() {
		CalculoBO calculoBO = new CalculoBO();
		Double media = 20d;
		String mensagemErroEsperada = "Média fora da faixa (0 a 10): 20,0";

		MediaForaFaixaException exception = assertThrows(MediaForaFaixaException.class, () -> {
			calculoBO.definirConceito(media);
		});
		String mensagemErroAtual = exception.getMessage();
		
		assertEquals(mensagemErroEsperada, mensagemErroAtual);

	}

	@Test
	@DisplayName("Testar definição de conceito para média -3.")
	public void testarDefinicaoConceitoMediaMenos3() {
		CalculoBO calculoBO = new CalculoBO();
		Double media = -3d;

		assertThrows(MediaForaFaixaException.class, () -> {
			calculoBO.definirConceito(media);
		});
	}

}
