package br.ucsal.bes20201.testequalidade.caderneta20201.suites;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.SelectClasses;
import org.junit.runner.RunWith;

import br.ucsal.bes20201.testequalidade.caderneta20201.business.integrado.CadernetaBOIntegradoTest;
import br.ucsal.bes20201.testequalidade.caderneta20201.business.unitario.CadernetaBOTest;
import br.ucsal.bes20201.testequalidade.caderneta20201.tui.CadernetaTUITest;

@RunWith(JUnitPlatform.class)
@SelectClasses({ CadernetaBOIntegradoTest.class, CadernetaBOTest.class, CadernetaTUITest.class })
public class CadernetaSuiteTest {

}
