package br.ucsal.testequalidade20162.business;

import java.time.LocalDateTime;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import br.ucsal.testequalidade20162.builder.CondutorBuilder;
import br.ucsal.testequalidade20162.builder.MultaBuilder;
import br.ucsal.testequalidade20162.domain.Condutor;
import br.ucsal.testequalidade20162.domain.Multa;
import br.ucsal.testequalidade20162.domain.TipoMultaEnum;

public class CondutorBOIntegradoTest {

	/*
	 * Caso de teste: # Entrada Saída esperada 1 Um condutor com 3 multas
	 * gravíssimas carteiraSuspensa = true 2 Um condutor com 2 multas leves e 3
	 * graves carteiraSuspensa = true
	 */

	/**
	 * Testar se um condutor com 21 pontos tem sua carteria considerada suspena.
	 * Caso de teste 2
	 */
	@Test
	public void testarVerificacaoCarteiraSuspensa21Pontos() {

		// Definir dados de entrada
		CondutorBuilder condutorBuilder = CondutorBuilder.umCondutor();

		MultaBuilder multaBuilder = MultaBuilder.umaMulta().comDataHora(2020, 5, 10, 12, 35).comLocal("Salvador")
														  .comTipo(TipoMultaEnum.GRAVE);

		Multa multa1 = multaBuilder.build();

		Multa multa2 = multaBuilder.mas().comDataHora(2020, 5, 10, 12, 30).build();

		Multa multa3 = multaBuilder.mas().comLocal("Barra").build();

		Multa multa4 = multaBuilder.mas().comTipo(TipoMultaEnum.LEVE).build();

		Multa multa5 = multaBuilder.mas().comTipo(TipoMultaEnum.LEVE).build();

		Condutor condutor1 = condutorBuilder.comMultas(multa1, multa2, multa3, multa4, multa5).build();

		// Definição de saída esperada, execução do método a ser testado, coleta da
		// saída atual e comparação da esperada com a atual.
		Assertions.assertTrue(CondutorBo.verificarCarteiraSuspensa(condutor1));

		// Não é interessante fazer assim:
		// // Definir a saída esperada
		// Boolean carteiraSuspensaEsperada = true;
		//
		// // Executar o método que está sendo testado e obter o saída atual
		// Boolean carteiraSuspensaAtual =
		// CondutorBo.verificarCarteiraSuspensa(condutor1);
		//
		// // Comparar a saída esperada com a saída atual
		// Assertions.assertEquals(carteiraSuspensaEsperada, carteiraSuspensaAtual);
	}

	public void testarVerificacaoCarteiraSuspensa21Pontos1() {

		// Definir dados de entrada
		Condutor condutor = new Condutor(1, "Claudio");

		MultaBuilder multaBuilder = MultaBuilder.umaMulta().comDataHora(2020, 10, 5, 12, 5).comLocal("Pituba")
				.comTipo(TipoMultaEnum.LEVE);

		Multa multa1 = new Multa();
		multa1.setDataHora(LocalDateTime.of(2020, 10, 5, 12, 5));
		multa1.setLocal("Pituba");
		multa1.setTipo(TipoMultaEnum.LEVE);

		Multa multa2 = new Multa();
		multa2.setDataHora(LocalDateTime.of(2020, 10, 5, 12, 15));
		multa2.setLocal("Pituba");
		multa2.setTipo(TipoMultaEnum.LEVE);

		Multa multa3 = new Multa();
		multa3.setDataHora(LocalDateTime.of(2020, 10, 5, 12, 25));
		multa3.setLocal("Pituba");
		multa3.setTipo(TipoMultaEnum.GRAVE);

		Multa multa4 = new Multa();
		multa4.setDataHora(LocalDateTime.of(2020, 10, 5, 12, 35));
		multa4.setLocal("Pituba");
		multa4.setTipo(TipoMultaEnum.GRAVE);

		Multa multa5 = new Multa();
		multa5.setDataHora(LocalDateTime.of(2020, 10, 5, 12, 55));
		multa5.setLocal("Pituba");
		multa5.setTipo(TipoMultaEnum.GRAVE);

		// Definir a saída esperada

		// Executar o método que está sendo testado e obter o saída atual

		// Comparar a saída esperada com a saída atual

	}

}
