package br.ucsal.testequalidade20162.builder;

import java.time.LocalDateTime;

import br.ucsal.testequalidade20162.domain.Multa;
import br.ucsal.testequalidade20162.domain.TipoMultaEnum;

public class MultaBuilder {

	private static final LocalDateTime DATAHORA_DEFAULT = LocalDateTime.now();
	private static final String LOCAL_DEFAULT = "Pituaçu";
	private static final TipoMultaEnum TIPO_DEFAULT = TipoMultaEnum.LEVE;

	private LocalDateTime dataHora = DATAHORA_DEFAULT;
	private String local = LOCAL_DEFAULT;
	private TipoMultaEnum tipo = TIPO_DEFAULT;

	private MultaBuilder() {

	}

	public static MultaBuilder umaMulta() {
		return new MultaBuilder();
	}

	public MultaBuilder comDataHora(Integer ano, Integer mes, Integer dia, Integer hora, Integer minuto) {
		this.dataHora = LocalDateTime.of(ano, mes, dia, hora, minuto);
		return this;
	}

	private MultaBuilder comDataHora(LocalDateTime dataHora) {
		this.dataHora = dataHora;
		return this;
	}
	
	public MultaBuilder comLocal(String local) {
		this.local = local;
		return this;
	}

	public MultaBuilder comTipo(TipoMultaEnum tipo) {
		this.tipo = tipo;
		return this;
	}
	
	public MultaBuilder mas() {
		return umaMulta().comLocal(local).comTipo(tipo).comDataHora(dataHora);
	}


	public Multa build() {
		Multa multa = new Multa();
		multa.setDataHora(dataHora);
		multa.setLocal(local);
		multa.setTipo(tipo);
		return multa;
	}

}
